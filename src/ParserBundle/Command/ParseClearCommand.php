<?php

namespace ParserBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ParserBundle\Entity\ParserNamespace;
use Doctrine\ORM\EntityManager;

class ParseClearCommand extends Command
{
    private $em;
    protected static $defaultName = 'parse:clear';

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct();
        $this->em = $entityManager;
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('parse:clear')

            // the short description shown while running "php bin/console list"
            ->setDescription('Truncate all parsed data from Symfony API request.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to clear all local Symfony API data:
            
    php bin/console parse:clear
            ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$em = $this->getContainer()->get('doctrine')->getManager();

        $namespace = $this->em->getRepository(ParserNamespace::class)->findAll();
        foreach ($namespace as $item) {
            $this->em->remove($item);
        }
        $this->em->flush();

        $output->writeln('You did it!');
    }
}

<?php

namespace AppBundle\GraphQL\Mutation;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;

class DeleteMutation implements MutationInterface, AliasedInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function deleting(int $id)
    {
        $article = $this->em->getRepository(Article::class)->find($id);
        if ($article) {
            $this->em->remove($article);
            $this->em->flush();
            $status = 'success';
        } else {
            $status = 'article is not exists';
        }

        return $status;
    }

    public static function getAliases()
    {
        return ['deleting' => 'Delete'];
    }
}

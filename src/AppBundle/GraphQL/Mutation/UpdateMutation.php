<?php

namespace AppBundle\GraphQL\Mutation;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Definition\Argument;

class UpdateMutation implements MutationInterface, AliasedInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function updating(Argument $args)
    {
        $article = $this->em->getRepository(Article::class)->find($args['id']);
        if ($article) {
            $article->setName($args['input']['name']);
            $article->setDescription($args['input']['description']);
            $article->setCreatedAt($args['input']['createdAt']);
            $this->em->persist($article);
            $this->em->flush();
        }

        return $article;
    }

    public static function getAliases()
    {
        return ['updating' => 'Update'];
    }
}

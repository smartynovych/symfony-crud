<?php

namespace AppBundle\GraphQL\Mutation;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Definition\Argument;

class CreateMutation implements MutationInterface, AliasedInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addition(Argument $args)
    {
        $article = new Article();
        $article->setName($args['input']['name']);
        $article->setDescription($args['input']['description']);
        $article->setCreatedAt($args['input']['createdAt']);

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    public static function getAliases()
    {
        return ['addition' => 'Create'];
    }
}

<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use AppBundle\Entity\Article;
use Gedmo\IpTraceable\IpTraceableListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class IpTraceSubscriber implements EventSubscriber
{
    /**
     * @var Request
     */
    private $request;
    protected $twig;
    protected $mailer;
    /**
     * @var IpTraceableListener
     */
    private $ipTraceableListener;

    public function __construct(IpTraceableListener $ipTraceableListener, \Twig_Environment $twig, \Swift_Mailer $mailer, Request $request = null)
    {
        $this->ipTraceableListener = $ipTraceableListener;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->request = $request;
    }

    /**
     * Set the username from the security context by listening on core.request.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (null === $this->request) {
            return;
        }

        $ip = $this->request->getClientIp();

        if (null !== $ip) {
            $this->ipTraceableListener->setIpValue($ip);
        }
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Article) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Article '.$entity->getId().' updated')
                ->setFrom('noreply@example.com')
                ->setTo('dev@example.com')
                ->setBody(
                    $this->twig->render(
                        'mail/article.html.twig',
                        array('name' => $entity->getName())
                    ),
                    'text/html'
                )
                ->addPart(
                    $this->twig->render(
                        'mail/article.txt.twig',
                        array('name' => $entity->getName())
                    ),
                    'text/plain'
                )
            ;
            $this->mailer->send($message);

            // Тут добавляем код для выполнения реакции на событие
            //$entityManager = $args->getEntityManager();
            //$article = $entityManager->getRepository(Article::class)->find(1);
        }
    }
}

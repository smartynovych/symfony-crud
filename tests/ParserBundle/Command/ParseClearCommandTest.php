<?php

namespace Tests\AppBundle\Command;

use ParserBundle\Command\ParseClearCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ParseClearCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = static::$kernel->getContainer()
            ->get(ParseClearCommand::class);

        $application->add($command);

        $command = $application->find('parse:clear');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array());

        $output = $commandTester->getDisplay();
        $this->assertContains('You did it!', $output);
    }
}
